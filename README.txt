README.txt
==========

A modern admin theme.

NOUVO TOOLBAR
=============

This theme works great with the Nouvo Toolbar module.

A NOT ABOUT CONTRIB MODULES
===========================

While an effort has been made to support popular contributed modules, it would
be silly to try to support every possible module. You can add your own styles
by creating a sub-theme or using the CSS Injector module.